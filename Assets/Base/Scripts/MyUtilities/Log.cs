﻿using UnityEngine;

public static class Log {
  /// <summary>
  /// <para>Logs a pFormatted simple message WITH TRANSLATION to the Unity console.</para>
  /// </summary>
  /// <param name="pFormat">A composite format string.</param>
  /// <param name="pArgs">Format arguments.</param>
  public static void Message (string pFormat, params object[] pArgs) {
    Debug.LogFormat(Tr.Go(pFormat), pArgs);
  }

  /// <summary>
  /// <para>Logs a pFormatted warning message WITH TRANSLATION to the Unity console.</para>
  /// </summary>
  /// <param name="pFormat">A composite format string.</param>
  /// <param name="pArgs">Format arguments.</param>
  public static void Warning (string pFormat, params object[] pArgs) {
    Debug.LogWarningFormat(Tr.Go(pFormat), pArgs);
  }

  /// <summary>
  /// <para>Logs a pFormatted error message WITH TRANSLATION to the Unity console.</para>
  /// </summary>
  /// <param name="pFormat">A composite format string.</param>
  /// <param name="pArgs">Format arguments.</param>
  public static void Error (string pFormat, params object[] pArgs) {
    Debug.LogErrorFormat(Tr.Go(pFormat), pArgs);
  }

  /// <summary>
  /// Log an error message and quit.
  /// </summary>
  /// <param name="pFormat">A composite format string.</param>
  /// <param name="pArgs">Format arguments.</param>
  public static void FatalError (string pFormat, params object[] pArgs) {
    Debug.LogErrorFormat(Tr.Go(pFormat), pArgs);
    Quit();
  }

  /// <summary>
  /// Log an error message and quit.
  /// </summary>
  /// <param name="TrueCondition">Condition that must be verified to avoid assertion.</param>
  /// <param name="pFormat">A composite format string.</param>
  /// <param name="pArgs">Format arguments.</param>
  public static void AssertFormat (bool TrueCondition, string pFormat, params object[] pArgs) {
    if (TrueCondition) return;
    Error(pFormat, pArgs);
  }

  /// <summary>
  /// Log an error message and quit.
  /// </summary>
  /// <param name="TrueCondition">Condition that must be verified to avoid assertion.</param>
  /// <param name="pFormat">A composite format string.</param>
  /// <param name="pArgs">Format arguments.</param>
  public static void AssertFormatQuit (bool TrueCondition, string pFormat, params object[] pArgs) {
    if (TrueCondition) return;
    FatalError(pFormat, pArgs);
  }

  /// <summary>
  /// Quit the application whatever the current mode is.
  /// </summary>
  public static void Quit () {
#if UNITY_EDITOR
    UnityEditor.EditorApplication.isPlaying = false;
#elif UNITY_WEBPLAYER
        Application.OpenURL("http://google.com");
#else
        Application.Quit();
#endif
  }
}

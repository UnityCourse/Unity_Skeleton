﻿public static class Tr {
    /// <summary>
    /// translate a string in a given language.
    /// </summary>
    /// <param name="pText">Text to translate</param>
    /// <param name="pLanguage">Language to translate in. "fr" (french) is default.</param>
    /// <returns>Translated text</returns>
    public static string Go (string pText, string pLanguage = "fr") {
        // TODO implement translation table
        // for now, just return the english string given in parameter
        return pText;
    }
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class EventSystemChecker : MonoBehaviour {
    public static EventSystem MenuEventSystem;

    // Any static function tagged with RuntimeInitializeOnLoadMethod will be called only a single time when the game load
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void InitSceneCallback () {
        SceneManager.sceneLoaded += SceneWasLoaded;
    }

    // This used to be OnLevelWasLoaded (now deprecated). It is registered as a callback on Scene Load in the
    // SceneManager in the function above.
    private static void SceneWasLoaded (Scene scene, LoadSceneMode mode) {
        //Find the EventSystem by type
        MenuEventSystem = FindObjectOfType<EventSystem>();

        //If there is no EventSystem (needed for UI interactivity) present
        if (MenuEventSystem != null) {
            return;
        }

        //The following code instantiates a new object called EventSystem
        var obj = new GameObject("EventSystem");

        //And adds the required components, while storing a reference in the menuSystem variable, which is static and accessible from other scripts
        MenuEventSystem = obj.AddComponent<EventSystem>();
        obj.AddComponent<StandaloneInputModule>().forceModuleActive = true;
    }
}

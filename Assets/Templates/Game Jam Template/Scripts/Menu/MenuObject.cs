﻿using UnityEngine;

public class MenuObject : MonoBehaviour {
    //Drag the object which you want to be automatically selected by the keyboard or gamepad when this panel becomes active
    public GameObject firstSelectedObject;

    public void SetFirstSelected () {
        //Tell the EventSystem to select this object
        EventSystemChecker.MenuEventSystem.SetSelectedGameObject(firstSelectedObject);
    }

    public void OnEnable () {
        //Check if we have an event system present
        if (EventSystemChecker.MenuEventSystem != null) {
            //If we do, select the specified object
            SetFirstSelected();
        }
    }
}
